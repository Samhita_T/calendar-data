# Objective:

The main objective of my work is to predict the stress level of an individual for a given time by considering certain features. 
Here, we have considered the heart-rate at a certain time,the performance level of the individual and the productivity. 
Required Data: Check-in Data, Heart-rate Data and  Calendar Data. 
Analyze the data and iedntify key factors that influence the performance and stress of an individual.
Develop Machine-Learning models to predict a meaningful outcome which assess the above discussed point. 

# Methodology:

1. Data Extraction.
2. Exploratory Data Analysis.
3. Data Cleaning.
4. Data Preparation.
5. Modeling and Validation.

# Extraction of Data: 

The check-in and heart-rate data have already been extracted and been merged on to their time frames. 
We have to work through the calendar data i.e extract the data, clean the data and provide a sophisticated data to predict the outcome. 

# Calendar Data

1. Understanding the basic working of a Google Calendar. 
2. A look at the structure of the Google Calendar. 
3. Understanding the basic working of an Outlook Calendar. 
4. A look at the structure of the Outlook Calendar. 





